import express from 'express'
import path from 'path'

const __dirname = path.resolve()
const app = express()
const PORT = 3000

app.listen(PORT, ()=>{console.log(`the server is running on port ${PORT}...`)})

app.get('/right', (req, res)=>{
    res.sendFile(path.resolve(__dirname, 'static', 'right.html'))
})
app.get('/home', (req, res)=>{
    res.sendFile(path.resolve(__dirname, 'static', 'home.html'))
})
app.get('/left', (req, res)=>{
    res.sendFile(path.resolve(__dirname, 'static', 'left.html'))
})